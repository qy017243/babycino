class TestBugH3 {
    public static void main(String[] args) {
        boolean condition = true;
        int i = 0;
        do {
            System.out.println("loop iterated");
            i++;
        } while (condition);
        System.out.println("loop terminated after " + i + " iterations");
    }
}
